Source: dcm4che
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>
Section: java
Priority: optional
Build-Depends: debhelper (>= 10),
               default-jdk,
               maven-debian-helper
Build-Depends-Indep: libcommons-cli-java,
                     liblog4j1.2-java,
                     libslf4j-java,
                     junit,
                     junit4,
                     default-jdk-doc,
                     liblog4j1.2-java-doc,
                     libmaven-javadoc-plugin-java
Standards-Version: 4.1.2
Vcs-Browser: https://salsa.debian.org/med-team/dcm4che
Vcs-Git: https://salsa.debian.org/med-team/dcm4che.git
Homepage: http://www.dcm4che.org/

Package: libdcm4che-java
Architecture: all
Depends: ${misc:Depends},
         ${maven:Depends}
Recommends: ${maven:OptionalDepends}
Suggests: libdcm4che-java-doc
Description: Clinical Image and Object Management
 Dcm4che is a collection of open source applications and utilities for
 the healthcare enterprise.
 .
 At the core of the dcm4che project is a robust implementation of the
 DICOM standard. The dcm4che DICOM toolkit is used in many production
 applications across the world.

Package: libdcm4che-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${maven:DocDepends}
Recommends: ${maven:DocOptionalDepends}
Suggests: libdcm4che-java
Description: Clinical Image and Object Management
 Dcm4che is a collection of open source applications and utilities for
 the healthcare enterprise.
 .
 At the core of the dcm4che project is a robust implementation of the
 DICOM standard. The dcm4che DICOM toolkit is used in many production
 applications across the world.
 .
 This package contains the API documentation of libdcm4che-core-java.
